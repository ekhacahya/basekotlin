package com.ekacahya.basekotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.ekacahya.basekotlin.R
import com.ekacahya.basekotlin.common.adapter.ViewType
import com.ekacahya.basekotlin.common.adapter.ViewTypeDelegateAdapter
import com.ekacahya.basekotlin.common.ext.inflate

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */
class LoadingDelegateAdapter : ViewTypeDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return LoadingViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {

    }

    class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.news_item_loading)) {
    }
}