package com.ekacahya.basekotlin.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.ekacahya.basekotlin.R
import com.ekacahya.basekotlin.common.adapter.ViewType
import com.ekacahya.basekotlin.common.adapter.ViewTypeDelegateAdapter
import com.ekacahya.basekotlin.common.ext.getFriendlyTime
import com.ekacahya.basekotlin.common.ext.inflate
import com.ekacahya.basekotlin.common.ext.loadImg
import com.ekacahya.basekotlin.data.model.RedditNewsItem
import kotlinx.android.synthetic.main.news_item.view.*

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */
class NewsDelegateAdapter(val listener: onViewSelectedListener) : ViewTypeDelegateAdapter {

    interface onViewSelectedListener {
        fun onItemSelected(url: String?)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return NewsViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as NewsViewHolder
        holder.bind(item as RedditNewsItem)
    }

    inner class NewsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.news_item)) {

        fun bind(item: RedditNewsItem) = with(itemView) {
            img_thumbnail.loadImg(item.thumbnail)
            description.text = item.title
            author.text = item.author
            comments.text = "${item.numComments} comments"
            time.text = item.created.getFriendlyTime()

            super.itemView.setOnClickListener { listener.onItemSelected(item.url) }
        }
    }

}
