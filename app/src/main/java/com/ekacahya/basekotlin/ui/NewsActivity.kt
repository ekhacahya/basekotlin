package com.ekacahya.basekotlin.ui

import android.os.Bundle
import com.ekacahya.basekotlin.R
import com.ekacahya.basekotlin.base.BaseActivity

class NewsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        if (savedInstanceState == null) {
            changeFragment(R.id.frame_news, NewsFrament())
        }
    }
}
