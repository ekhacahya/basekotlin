package com.ekacahya.basekotlin.ui

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment.getExternalStorageDirectory
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ekacahya.basekotlin.R
import com.ekacahya.basekotlin.adapter.NewsAdapter
import com.ekacahya.basekotlin.adapter.NewsDelegateAdapter
import com.ekacahya.basekotlin.base.BaseRxFragment
import com.ekacahya.basekotlin.common.ext.inflate
import com.ekacahya.basekotlin.common.ext.navigateTo
import com.ekacahya.basekotlin.common.feature.InfiniteScrollListener
import com.ekacahya.basekotlin.data.manager.NewsDataManager
import com.ekacahya.basekotlin.data.model.RedditNews
import kotlinx.android.synthetic.main.fragment_news.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import permissions.dispatcher.RuntimePermissions
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File


/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */

@RuntimePermissions
class NewsFrament : BaseRxFragment(), NewsDelegateAdapter.onViewSelectedListener {
    override fun onItemSelected(url: String?) {
        toast(url.orEmpty())
    }

    companion object {
        private val KEY_REDDIT_NEWS = "redditNews"
    }

    private var itemNews: RedditNews? = null
    private val dataManager by lazy { NewsDataManager() }
    private val newsList by lazy {
        recyclerView.apply {
            setHasFixedSize(true)
            val linearLayout = LinearLayoutManager(context)
            layoutManager = linearLayout
            addOnScrollListener(InfiniteScrollListener({ requestMoreNews() }, linearLayout))
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_news)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fabInit()
        initAdapter()
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_REDDIT_NEWS)) {
            itemNews = savedInstanceState.get(KEY_REDDIT_NEWS) as RedditNews
            (newsList.adapter as NewsAdapter).clearAndAddNews(itemNews!!.news)
        } else {
            requestMoreNews()
        }
    }

    private fun fabInit() {
        fab.setOnClickListener {
            activity.navigateTo(NewsActivity::class.java)
        }
    }

    private val PICK_CAMERA_IMAGE: Int = 1

    @NeedsPermission(Manifest.permission.CAMERA)
    fun launchCamera() {
        val name = System.currentTimeMillis()
        val destination = File(getExternalStorageDirectory(), "${name}.jpg")

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(destination))
        startActivityForResult(intent, PICK_CAMERA_IMAGE)
    }

    @OnShowRationale(Manifest.permission.CAMERA)
    fun showRationaleForCamera(request: PermissionRequest) {
        AlertDialog.Builder(activity)
                .setMessage(R.string.permission_camera_rationale)
                .setPositiveButton(R.string.button_allow, { dialog, button -> request.proceed() })
                .setNegativeButton(R.string.button_deny, { dialog, button -> request.cancel() })
                .show()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val news = (newsList.adapter as NewsAdapter).getNews()
        if (itemNews != null && news.isNotEmpty()) {
            outState.putParcelable(KEY_REDDIT_NEWS, itemNews?.copy(news = news))
        }

    }

    private fun requestMoreNews() {
        val subscription = dataManager
                .getNews(itemNews?.after ?: "")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { retrievedNews ->
                            itemNews = retrievedNews
                            (newsList.adapter as NewsAdapter).addNews(retrievedNews.news)
                        },
                        { e ->
                            if (view != null) {
                                toast(e.message ?: "")
                            }
                        }
                )
        subscriptions.add(subscription)
    }

    private fun initAdapter() {
        if (recyclerView.adapter == null) {
            recyclerView.adapter = NewsAdapter(this)
        }
    }
}