@file:JvmName("ExtensionsUtils")

package com.ekacahya.basekotlin.common.ext

import android.content.Context
import android.graphics.Point
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.ekacahya.basekotlin.R

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun ImageView.loadImg(imageUrl: String) {
    if (TextUtils.isEmpty(imageUrl)) {
        Glide.with(context).load(R.mipmap.ic_launcher).into(this)
    } else {
        Glide.with(context).load(imageUrl).into(this)
    }
}

fun Context.shortToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Context.longToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}


inline fun <reified T : Any> T.logD(msg: Any, tag: String = tag()) {
    Log.d(tag, msg.toString())
}

inline fun <reified T : Any> T.logI(msg: Any, tag: String = tag()) {
    Log.i(tag, msg.toString())
}

inline fun <reified T : Any> T.logV(msg: Any, tag: String = tag()) {
    Log.v(tag, msg.toString())
}

inline fun <reified T : Any> T.logW(msg: Any, tag: String = tag()) {
    Log.w(tag, msg.toString())
}

inline fun <reified T : Any> T.logE(msg: Any, tag: String = tag()) {
    Log.e(tag, msg.toString())
}

fun getScreenWidth(context: Context) = getSizeOfDisplay(context).x

fun getScreenHeight(context: Context) = getSizeOfDisplay(context).y

private fun getSizeOfDisplay(context: Context): Point {
    val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = windowManager.defaultDisplay
    var p = Point()
    display.getSize(p)
    return p
}

inline fun <reified T : Any> T.tag() = T::class.java.simpleName

fun <T> unSafeLazy(initializer: () -> T): Lazy<T> {
    return lazy(LazyThreadSafetyMode.NONE, initializer)
}