package com.ekacahya.basekotlin.common.adapter

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */
object AdapterConstants {
    val NEWS = 1
    val LOADING = 2
}