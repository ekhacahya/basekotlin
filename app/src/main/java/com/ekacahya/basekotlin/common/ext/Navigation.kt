package com.ekacahya.basekotlin.common.ext

import android.app.Activity
import android.content.Intent


/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 6/3/17.
 */

//activity.navigateTo(NewsActivity::class.java)
fun Activity.navigateTo(clazz: Class<*>) {
    val intent = Intent(this, clazz::class.java)
    startActivity(intent)
}