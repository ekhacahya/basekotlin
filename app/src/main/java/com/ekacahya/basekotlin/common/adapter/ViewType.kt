package com.ekacahya.basekotlin.common.adapter

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */
interface ViewType {
    fun getViewType(): Int
}