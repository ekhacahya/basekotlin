package com.ekacahya.basekotlin.data.manager

import com.ekacahya.basekotlin.data.api.ApiFactory
import com.ekacahya.basekotlin.data.api.NewsAPI
import com.ekacahya.basekotlin.data.model.RedditNews
import com.ekacahya.basekotlin.data.model.RedditNewsItem
import rx.Observable

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */
class NewsDataManager(val api: NewsAPI = ApiFactory()) {

    fun getNews(after: String, limit: String = "20"): Observable<RedditNews> {
        return Observable.create {
            subscriber ->
            val callResponse = api.getNews(after, limit)
            val response = callResponse.execute()

            if (response.isSuccessful) {
                val news = response.body().children.map {
                    val item = it
                    RedditNewsItem(item.author, item.title, item.numComments,
                            item.created, item.thumbnail, item.url)
                }
                subscriber.onNext(
                        RedditNews(
                                response.body().after ?: "",
                                response.body().before ?: "",
                                news))
                subscriber.onCompleted()
            } else {
                subscriber.onError(Throwable(response.message()))
            }
        }
    }

}