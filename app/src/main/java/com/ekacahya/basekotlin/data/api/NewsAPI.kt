package com.ekacahya.basekotlin.data.api

import com.ekacahya.basekotlin.data.model.RedditNewsResponse
import retrofit2.Call

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/5/17.
 */
interface NewsAPI {
    fun getNews(after: String, limit: String): Call<RedditNewsResponse>
}