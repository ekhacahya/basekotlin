package com.ekacahya.basekotlin.data.model

/**
 * BaseKotlin
 * Created by Eka Cahya Budhi N
 * ekhacahya@live.com
 * ekacahya.com
 * on 5/17/17.
 */
data class RedditNewsResponse(val before: String?, val after: String?, val children: List<RedditNewsItem>)